//Laptop
interface Laptop {
  readonly ancho: number;
  readonly alto: number;
  readonly marca: string;
  imprimirDatos():void
}

let laptop: Laptop = {
  ancho: 5,
  alto: 6,
  marca: "toshiba",
  imprimirDatos():void{
    console.log(`ancho: ${this.ancho}`);
    console.log(`alto: ${this.alto}`);
    console.log(`marca: ${this.marca}`);
  }
}
console.log("los datos de mi laptop son:");
laptop.imprimirDatos()
//Pizarra
interface Pizarra {
  readonly ancho: number;
  readonly alto: number;
  readonly marca: string;
  imprimirDatos():void
}

let pizarra: Pizarra = {
  ancho: 5,
  alto: 6,
  marca: "cosolizi",
  imprimirDatos():void{
    console.log(`ancho: ${this.ancho}`);
    console.log(`alto: ${this.alto}`);
    console.log(`marca: ${this.marca}`);
  }
}
console.log("los datos de mi pizarra son:");
pizarra.imprimirDatos()
//Televisor
interface Televisor {
  readonly pixeles:number;
  readonly pulgada: number;
  readonly marca: string;
  imprimirDatos():void
}

let tele: Televisor = {
  pixeles: 50,
  pulgada: 20,
  marca: "LG",
  imprimirDatos():void{
    console.log(`pixeles: ${this.pixeles}`);
    console.log(`pulgadas: ${this.pulgada}`);
    console.log(`marca: ${this.marca}`);
  }
}
console.log("los datos de mi tele son:");
tele.imprimirDatos()
