"use strict";
let laptop = {
    ancho: 5,
    alto: 6,
    marca: "toshiba",
    imprimirDatos() {
        console.log(`ancho: ${this.ancho}`);
        console.log(`alto: ${this.alto}`);
        console.log(`marca: ${this.marca}`);
    }
};
console.log("los datos de mi laptop son:");
laptop.imprimirDatos();
let pizarra = {
    ancho: 5,
    alto: 6,
    marca: "cosolizi",
    imprimirDatos() {
        console.log(`ancho: ${this.ancho}`);
        console.log(`alto: ${this.alto}`);
        console.log(`marca: ${this.marca}`);
    }
};
console.log("los datos de mi pizarra son:");
pizarra.imprimirDatos();
let tele = {
    pixeles: 50,
    pulgada: 20,
    marca: "LG",
    imprimirDatos() {
        console.log(`pixeles: ${this.pixeles}`);
        console.log(`pulgadas: ${this.pulgada}`);
        console.log(`marca: ${this.marca}`);
    }
};
console.log("los datos de mi tele son:");
tele.imprimirDatos();
