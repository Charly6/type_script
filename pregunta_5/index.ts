function rest1(...letras:string[]): void {
  console.log(letras);
}
rest1("hola", "como", "estas")

function rest2(num:number, ...letras:string[]): void {
  console.log(`numero es: ${num}
letras son: ${letras}`);
}
rest2(2, "hola","como","estas")
