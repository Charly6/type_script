"use strict";
function serie(n) {
    let result = [];
    let limite = 1;
    let contador = 0;
    let agregando = true;
    function comun() {
        contador = 0;
        agregando = !agregando;
    }
    while (n > result.length) {
        contador++;
        if (agregando) {
            result.push(0);
        }
        else {
            result.push(1);
        }
        if (limite === contador && !agregando) {
            limite++;
            comun();
        }
        else if (limite === contador) {
            comun();
        }
    }
    console.log('' + result);
}
console.log('test con 15');
serie(15);
console.log('');
console.log('test con 4');
serie(4);
console.log('');
console.log('test con 7');
serie(7);
console.log('');
console.log('test con 20');
serie(30);
console.log('');
