function serie(n:number):void {
  let result: number[] = []
  let limite: number = 1;
  let contador: number = 0;
  let agregando: boolean = true;
  function comun():void {
    contador=0
    agregando = !agregando
  }

  while(n > result.length){
    contador++
    if(agregando){
      result.push(0)
    }else{
      result.push(1)
    }

    if(limite === contador && !agregando) {
      limite++
      comun();
    } else if(limite === contador) {
      comun()
    }
  }
  console.log(''+result);
}

console.log('test con 15');
serie(15)
console.log('');

console.log('test con 4');
serie(4)
console.log('');

console.log('test con 7');
serie(7)
console.log('');

console.log('test con 20');
serie(30)
console.log('');
