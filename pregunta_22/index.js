"use strict";
function serie(n) {
    let result = [];
    let repeticiones = 1;
    let num = 2;
    while (n > result.length) {
        result.push(num);
        if (repeticiones === num) {
            repeticiones = 0;
            num = num + 2;
        }
        repeticiones++;
    }
    console.log('' + result);
}
console.log('test con 12');
serie(12);
console.log('');
console.log('test con 21');
serie(21);
console.log('');
console.log('test con 45');
serie(45);
console.log('');
