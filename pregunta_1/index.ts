let a: [number, number, number] = [2, 3, 5];
let b: [number, number, number] = [8, 9, 11];
console.log(`Dado los arreglos [${a}] y [${b}]`);

let c: [number, number, number?] = [a[0] + b[0], a[1] + b[1], a[2] + b[2]];
console.log(`a) es igual: [${c}]`);

for(let i=0; i<3; i++){
  c[i] = a[i] + b[0] + b[1] + b[2];
}
console.log(`b) es igual: [${c}]`);
//---------------
c = [a[0] * b[0], a[1] * b[1], a[2] * b[2]];
console.log(`c) es igual: [${c}]`);

for(let i=0; i<3; i++){
  c[i] = a[i] * b[0] * b[1] * b[2];
}
console.log(`d) es igual: [${c}]`);

c = [a.length + b.length, a[0] + b[0] + a[1] + b[1] + a[2] + b[2], ]
console.log(`e) es igual: [${c}]`);
