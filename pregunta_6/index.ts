//Perro
interface Perro{
  nombre:string;
  raza:string;
  edad:number;
  imprimirNombre():void
  imprimirRaza():void
  imprimirEdad():void
}

let perro: Perro = 
{
  nombre : "terry",
  raza : "chiwawa",
  edad : 3,
  imprimirNombre():void{
    console.log(`nombre: ${this.nombre}`);
  },
  
  imprimirRaza():void{
    console.log(`raza: ${this.raza}`);
  },
  imprimirEdad():void{
    console.log(`edad: ${this.edad}`);
  }
}
console.log(`Las propiedades de mi perro son:`);
perro.imprimirNombre()
perro.imprimirEdad()
perro.imprimirRaza()
//Gato
interface Gato{
  nombre:string;
  raza:string;
  edad:number;
  imprimirNombre():void
  imprimirRaza():void
  imprimirEdad():void
}

let gato: Gato = 
{
  nombre : "michi",
  raza : "egipcio",
  edad : 2,
  imprimirNombre():void{
    console.log(`nombre: ${this.nombre}`);
  },
  
  imprimirRaza():void{
    console.log(`raza: ${this.raza}`);
  },
  imprimirEdad():void{
    console.log(`edad: ${this.edad}`);
  }
}
console.log(`Las propiedades de mi gato son:`);
gato.imprimirNombre()
gato.imprimirEdad()
gato.imprimirRaza()
//Persona
interface Persona{
  nombre:string;
  apellido:string;
  edad:number;
  imprimirNombre():void
  imprimirApellido():void
  imprimirEdad():void
}

let persona : Persona = {
  nombre:"charly",
  apellido:"meneces",
  edad:23,
  imprimirNombre():void{
    console.log(`nombre: ${this.nombre}`);
  },
  imprimirApellido():void{
    console.log(`apellido: ${this.apellido}`);
  },
  imprimirEdad():void{
    console.log(`edad: ${this.edad}`);
  },
}
console.log(`Las propiedades de mi persona son:`);
persona.imprimirNombre()
persona.imprimirApellido()
persona.imprimirEdad()
//Mesa
interface Mesa {
  ancho: number;
  alto: number;
  material:string;
  imprimirAncho():void
  imprimirAlto():void
  imprimirMaterial():void
}

let mesa : Mesa = {
  ancho: 5,
  alto: 10,
  material:"madera",
  imprimirAncho():void{
    console.log(`ancho: ${this.ancho}`)
  },
  imprimirAlto():void{
    console.log(`alto: ${this.alto}`)
  },
  imprimirMaterial():void{
    console.log(`material: ${this.material}`)
  }
}
console.log(`Las propiedades de mi mesa son:`);
mesa.imprimirAncho()
mesa.imprimirAlto()
mesa.imprimirMaterial()
//Silla
interface Silla {
  ancho:number;
  alto:number;
  material:string;
  imprimirAncho():void
  imprimirAlto():void
  imprimirMaterial():void
}

let silla : Silla = {
  ancho:10,
  alto:12,
  material:"madera",
  imprimirAncho():void{
    console.log(`ancho: ${this.ancho}`)
  },
  imprimirAlto():void{
    console.log(`alto: ${this.alto}`)
  },
  imprimirMaterial():void{
    console.log(`material: ${this.material}`)
  }
}
console.log(`Las propiedades de mi silla son:`);
mesa.imprimirAncho()
mesa.imprimirAlto()
mesa.imprimirMaterial()
