"use strict";
let perro = {
    nombre: "terry",
    raza: "chiwawa",
    edad: 3,
    imprimirNombre() {
        console.log(`nombre: ${this.nombre}`);
    },
    imprimirRaza() {
        console.log(`raza: ${this.raza}`);
    },
    imprimirEdad() {
        console.log(`edad: ${this.edad}`);
    }
};
console.log(`Las propiedades de mi perro son:`);
perro.imprimirNombre();
perro.imprimirEdad();
perro.imprimirRaza();
let gato = {
    nombre: "michi",
    raza: "egipcio",
    edad: 2,
    imprimirNombre() {
        console.log(`nombre: ${this.nombre}`);
    },
    imprimirRaza() {
        console.log(`raza: ${this.raza}`);
    },
    imprimirEdad() {
        console.log(`edad: ${this.edad}`);
    }
};
console.log(`Las propiedades de mi gato son:`);
gato.imprimirNombre();
gato.imprimirEdad();
gato.imprimirRaza();
let persona = {
    nombre: "charly",
    apellido: "meneces",
    edad: 23,
    imprimirNombre() {
        console.log(`nombre: ${this.nombre}`);
    },
    imprimirApellido() {
        console.log(`apellido: ${this.apellido}`);
    },
    imprimirEdad() {
        console.log(`edad: ${this.edad}`);
    },
};
console.log(`Las propiedades de mi persona son:`);
persona.imprimirNombre();
persona.imprimirApellido();
persona.imprimirEdad();
let mesa = {
    ancho: 5,
    alto: 10,
    material: "madera",
    imprimirAncho() {
        console.log(`ancho: ${this.ancho}`);
    },
    imprimirAlto() {
        console.log(`alto: ${this.alto}`);
    },
    imprimirMaterial() {
        console.log(`material: ${this.material}`);
    }
};
console.log(`Las propiedades de mi mesa son:`);
mesa.imprimirAncho();
mesa.imprimirAlto();
mesa.imprimirMaterial();
let silla = {
    ancho: 10,
    alto: 12,
    material: "madera",
    imprimirAncho() {
        console.log(`ancho: ${this.ancho}`);
    },
    imprimirAlto() {
        console.log(`alto: ${this.alto}`);
    },
    imprimirMaterial() {
        console.log(`material: ${this.material}`);
    }
};
console.log(`Las propiedades de mi silla son:`);
mesa.imprimirAncho();
mesa.imprimirAlto();
mesa.imprimirMaterial();
