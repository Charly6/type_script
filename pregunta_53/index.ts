function histograma(cadena:number[]):void {
  let result = {
    uno: 0,
    dos: 0,
    tres: 0,
    cuatro: 0,
    cinco: 0
  }
  for (let index = 0; index < cadena.length; index++) {
    cadena[index] === 1? result.uno++:
    cadena[index] === 2? result.dos++:
    cadena[index] === 3? result.tres++:
    cadena[index] === 4? result.cuatro++:
    cadena[index] === 5? result.cinco++:
    ''
  }
  console.log(result);
  
}

console.log('test con [1, 4, 3, 4, 1, 5, 1, 1, 5, 2]');
histograma([1, 4, 3, 4, 1, 5, 1, 1, 5, 2])
console.log('');

console.log('test con [1, 4, 2, 5, 2, 1, 1, 1, 3, 2]');
histograma([1, 4, 2, 5, 2, 1, 1, 1, 3, 2])
console.log('');

console.log('test con [1, 4, 5, 3, 2, 5, 4, 3, 2, 5]');
histograma([1, 4, 5, 3, 2, 5, 4, 3, 2, 5])
console.log('');
