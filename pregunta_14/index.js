"use strict";
class Hacer {
    static suma() {
        console.log(this.num1 + this.num2);
    }
    static resta() {
        console.log(this.num1 - this.num2);
    }
    static multiplicacion() {
        console.log(this.num1 * this.num2);
    }
    static divicion() {
        console.log(this.num1 / this.num2);
    }
    static modulo() {
        console.log(this.num1 % this.num2);
    }
}
console.log('Definiendo los numeros 10 y 5');
Hacer.num1 = 10;
Hacer.num2 = 5;
console.log('Ejecutando la suma');
Hacer.suma();
console.log('Ejecutando la resta');
Hacer.resta();
console.log('Ejecutando la multiplicacion');
Hacer.multiplicacion();
console.log('Ejecutando la divicion');
Hacer.divicion();
console.log('Ejecutando la modulo');
Hacer.modulo();
