let perro1: [string, string, string, number] = ["terry", "azul", "bullterrier",4];
let perro2: [string, string, string, number] = ["boby", "negro", "pitbull",2];
let perro3: [string, string, string, number] = ["palto", "cafe", "chiwawa",8];

let gato1: [string, string, string, number] = ["michi", "cafe", "persa",8];
let gato2: [string, string, string, number] = ["pst", "blanco", "europeo",7];
let gato3: [string, string, string, number] = ["tigre", "negro", "egipcio",1];

function print(tupla:[string, string, string,number]) {
  console.log(`El gato ${tupla[0]} de color ${tupla[1]} y raza ${tupla[2]} tiene ${tupla[3]} años`);
}

//perros
print(perro1)
print(perro2)
print(perro3)

//gatos
print(gato1)
print(gato2)
print(gato3)
