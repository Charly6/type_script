"use strict";
//Pc de escritorio
class PcEscritorio {
    get precio() {
        if (this._precio === undefined) {
            return 0;
        }
        else {
            return this._precio;
        }
    }
    get tamano() {
        if (this._tamano === undefined) {
            return 0;
        }
        else {
            return this._tamano;
        }
    }
    set precio(precio_) {
        if (this._precio != undefined && this._precio > precio_) {
            this._precio = precio_;
        }
    }
    set tamano(tamano_) {
        if (tamano_ != this._tamano) {
            this._tamano = tamano_;
        }
    }
}
let pc = new PcEscritorio();
console.log('Clase pc de escritorio (con get y set)');
console.log(pc);
console.log('');
//Cocina
class Cocina {
    get sillas() {
        if (this._sillas === undefined) {
            return 0;
        }
        else {
            return this._sillas;
        }
    }
    get mesa() {
        if (this._mesa === undefined) {
            return false;
        }
        else {
            return this._mesa;
        }
    }
    set silas(silas_) {
        if (this._sillas != undefined && this._sillas != silas_) {
            this._sillas = silas_;
        }
    }
    set mesa(mesa_) {
        if (this._mesa != undefined && this._mesa != mesa_) {
            this._mesa = mesa_;
        }
    }
}
let cocina = new Cocina();
console.log('Clase cocina (con get y set)');
console.log(cocina);
console.log('');
//Farmacia
class Farmacia {
    get alcohol() {
        if (this._alcohol != undefined) {
            return this._alcohol;
        }
        else {
            return 0;
        }
    }
    get barbijos() {
        if (this._barbijos != undefined) {
            return this._barbijos;
        }
        else {
            return 0;
        }
    }
    set alcohol(alcohol_) {
        if (alcohol_ != this._alcohol) {
            this._alcohol = alcohol_;
        }
    }
    set barbijos(barbijos_) {
        if (barbijos_ != this._barbijos) {
            this._barbijos = barbijos_;
        }
    }
}
let farmacia = new Farmacia();
console.log('Clase farmacia (con get y set)');
console.log(farmacia);
console.log('');
