//Pc de escritorio
class PcEscritorio {
  private _precio?: number;
  private _tamano?: number;

  public get precio(){
    if(this._precio===undefined){
      return 0;
    }else {
    return this._precio;
    }
  }
  
  public get tamano(){
    if(this._tamano===undefined){
      return 0;
    }else {
    return this._tamano;
    }
  }

  public set precio(precio_:number){
    if(this._precio!=undefined && this._precio > precio_){
      this._precio = precio_;
    }
  }

  public set tamano(tamano_:number){
    if(tamano_ != this._tamano){
      this._tamano = tamano_;
    }
  }
}
  let pc = new PcEscritorio()
  console.log('Clase pc de escritorio (con get y set)');
  console.log(pc);
  console.log('');
//Cocina
class Cocina{
  _sillas?:number;
  _mesa?:boolean;

  public get sillas(){
    if(this._sillas===undefined){
      return 0;
    }else {
    return this._sillas;
    }
  }

  public get mesa(){
    if(this._mesa===undefined){
      return false;
    }else {
    return this._mesa;
    }
  }
  
  public set silas(silas_:number){
    if(this._sillas != undefined && this._sillas != silas_){
      this._sillas = silas_;
    }
  }
  
  public set mesa(mesa_:boolean){
    if(this._mesa != undefined && this._mesa != mesa_){
      this._mesa = mesa_;
    }
  }
}
let cocina = new Cocina()
console.log('Clase cocina (con get y set)');
console.log(cocina);
console.log('');
//Farmacia
class Farmacia{
  _alcohol?:number;
  _barbijos?:number;

  public get alcohol(){
    if(this._alcohol != undefined){
      return this._alcohol;
    }else{
      return 0;
    }
  }
  
  public get barbijos(){
    if(this._barbijos != undefined){
      return this._barbijos
    }else {
      return 0;
    }
  }

  public set alcohol(alcohol_:number){
    if(alcohol_ != this._alcohol){
      this._alcohol=alcohol_;
    }
  }

  public set barbijos(barbijos_:number){
    if(barbijos_!=this._barbijos){
      this._barbijos=barbijos_;
    }
  }
}
let farmacia = new Farmacia()
console.log('Clase farmacia (con get y set)');
console.log(farmacia);
console.log('');
