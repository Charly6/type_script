"use strict";
//clase 1
class MyObjeto1 {
    get atributo1() {
        return this._atributo1;
    }
    get atributo2() {
        return this._atributo2;
    }
}
//clase 2
class MyObjeto2 extends MyObjeto1 {
    get atributo3() {
        return this._atributo3;
    }
    get atributo4() {
        return this._atributo4;
    }
}
//clase 3
class MyObjeto3 extends MyObjeto2 {
    get atributo5() {
        return this._atributo5;
    }
}
