
//clase 1
class MyObjeto1 {
  private _atributo1?: number;
  private _atributo2?: number;
  protected _atributoCompartido?:string;

  public get atributo1(){
    return this._atributo1
  }

  public get atributo2(){
    return this._atributo2
  }
}
//clase 2
class MyObjeto2 extends MyObjeto1{
  private _atributo3?: number;
  private _atributo4?: number;
  protected _atributoCompartido2?:string;

  public get atributo3(){
    return this._atributo3
  }

  public get atributo4(){
    return this._atributo4
  }
}
//clase 3
class MyObjeto3 extends MyObjeto2{
  private _atributo5?:number;

  public get atributo5(){
    return this._atributo5
  }
}
