const funcionJs = (parametro) => {
  parametro === undefined? console.log("parametro indefinido"):
  console.log("parametro definido");
}

function funcionTs(parametro?:string): void {
  parametro === undefined? console.log("parametro indefinido"):
  console.log("parametro definido");
}

funcionTs();
funcionTs("")