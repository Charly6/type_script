//clase 1
class MiObjeto1 {
  private _atributo1?: number;
  private _atributo2?: number;

  public get atributo1(){
    return this._atributo1
  }

  public get atributo2(){
    return this._atributo2
  }
}
//clase 2
class MiObjeto2 {
  private _atributo1?: number;
  private _atributo2?: number;

  public get atributo1(){
    return this._atributo1
  }

  public get atributo2(){
    return this._atributo2
  }
}
