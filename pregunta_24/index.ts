function serie(k:number, n:number):void {

  let result: number[] = []
  let iteracion : boolean = true

  while(k > result.length){
    if(iteracion){
      result.push(1)
    }else {
      result.push(0)
    }
    ((result.length % n) === 0)?iteracion = !iteracion : 0;
  }
  console.log(''+result);
}

console.log('test con 15, 4');
serie(15, 4)
console.log('');

console.log('test con 10, 4');
serie(10, 4)
console.log('');

console.log('test con 21, 5');
serie(21, 5)
console.log('');
