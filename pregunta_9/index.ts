//Edificio
interface Edificio {
  nombre: string;
  pisos: number;
  ancho:number;
  largo: number;
  imprimirDatos():void
  validar():void;
}

let edificio: Edificio = {
  nombre: "torres sofer",
  pisos: 12,
  ancho: 20,
  largo: 25,
  imprimirDatos():void{
    console.log(`nombre: ${this.nombre}`);
    console.log(`pisos: ${this.pisos}`);
    console.log(`ancho: ${this.ancho}`);
    console.log(`largo: ${this.largo}`);
  },
  validar():void{
    if(this.nombre.length > 12){
      console.log("El nombre es demaciado largo por favor cambielo");
    }else {
      console.log("El nombre es el indicado :)");
    }
  }
}
console.log('Datos de mi edificio son:');
edificio.imprimirDatos()
console.log('validando el nombre:');
edificio.validar()
console.log('');

//Colegio
interface Colegio {
  nombre:string;
  cursos:number;
  estudiantes:number;
  imprimirDatos():void
  validar():void;
}

let colegio : Colegio = {
  nombre:"Albert Einstein",
  cursos: 8,
  estudiantes: 300,
  imprimirDatos():void {
    console.log(`nombre: ${this.nombre}`);
    console.log(`cursos: ${this.cursos}`);
    console.log(`estudiantes: ${this.estudiantes}`);
  },
  validar():void {
    if(this.estudiantes/this.cursos < 40){
      console.log("El colegio tiene pocos estudiantes");
    } else {
      console.log("El colegio tiene muchos estudiantes")
    }
  }
}
console.log('Datos de mi colegio son:')
colegio.imprimirDatos()
console.log('Validando estudiantes:');
colegio.validar()
console.log('');

//Mochila
interface Mochila {
  color:string;
  marca:string;
  bolsillos:number;
  imprimirDatos():void
  validar():void;
}

let mochila : Mochila = {
  color:"plomo",
  marca: "totto",
  bolsillos: 4,
  imprimirDatos():void {
    console.log(`color: ${this.color}`);
    console.log(`marca: ${this.marca}`);
    console.log(`bolsillos: ${this.bolsillos}`);
  },
  validar():void {
    if(this.bolsillos < 5){
      console.log("La mochila es pequeña");
    } else {
      console.log("La mochila es grande");
    }
  }
}
console.log('Datos de mi mochila son:')
mochila.imprimirDatos()
console.log('Validando tamaño deacuerdo a los bolsillos:');
mochila.validar()
console.log('');
//Cuaderno
interface Cuaderno {
  color:string;
  marca:string;
  hojas:number;
  imprimirDatos():void
  validar():void;
}

let cuaderno : Cuaderno = {
  color:"plomo",
  marca: "totto",
  hojas: 100,
  imprimirDatos():void {
    console.log(`color: ${this.color}`);
    console.log(`marca: ${this.marca}`);
    console.log(`hojas: ${this.hojas}`);
  },
  validar():void {
    if(this.hojas < 50){
      console.log("El cuaderno tiene pocas hojas");
    } else {
      console.log("El cuaderno tiene muchas hojas");
    }
  }
}
console.log('Datos de mi cuaderno son:')
cuaderno.imprimirDatos()
console.log('Validando hojas:');
cuaderno.validar()