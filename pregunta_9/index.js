"use strict";
let edificio = {
    nombre: "torres sofer",
    pisos: 12,
    ancho: 20,
    largo: 25,
    imprimirDatos() {
        console.log(`nombre: ${this.nombre}`);
        console.log(`pisos: ${this.pisos}`);
        console.log(`ancho: ${this.ancho}`);
        console.log(`largo: ${this.largo}`);
    },
    validar() {
        if (this.nombre.length > 12) {
            console.log("El nombre es demaciado largo por favor cambielo");
        }
        else {
            console.log("El nombre es el indicado :)");
        }
    }
};
console.log('Datos de mi edificio son:');
edificio.imprimirDatos();
console.log('validando el nombre:');
edificio.validar();
console.log('');
let colegio = {
    nombre: "Albert Einstein",
    cursos: 8,
    estudiantes: 300,
    imprimirDatos() {
        console.log(`nombre: ${this.nombre}`);
        console.log(`cursos: ${this.cursos}`);
        console.log(`estudiantes: ${this.estudiantes}`);
    },
    validar() {
        if (this.estudiantes / this.cursos < 40) {
            console.log("El colegio tiene pocos estudiantes");
        }
        else {
            console.log("El colegio tiene muchos estudiantes");
        }
    }
};
console.log('Datos de mi colegio son:');
colegio.imprimirDatos();
console.log('Validando estudiantes:');
colegio.validar();
console.log('');
let mochila = {
    color: "plomo",
    marca: "totto",
    bolsillos: 4,
    imprimirDatos() {
        console.log(`color: ${this.color}`);
        console.log(`marca: ${this.marca}`);
        console.log(`bolsillos: ${this.bolsillos}`);
    },
    validar() {
        if (this.bolsillos < 5) {
            console.log("La mochila es pequeña");
        }
        else {
            console.log("La mochila es grande");
        }
    }
};
console.log('Datos de mi mochila son:');
mochila.imprimirDatos();
console.log('Validando tamaño deacuerdo a los bolsillos:');
mochila.validar();
console.log('');
let cuaderno = {
    color: "plomo",
    marca: "totto",
    hojas: 100,
    imprimirDatos() {
        console.log(`color: ${this.color}`);
        console.log(`marca: ${this.marca}`);
        console.log(`hojas: ${this.hojas}`);
    },
    validar() {
        if (this.hojas < 50) {
            console.log("El cuaderno tiene pocas hojas");
        }
        else {
            console.log("El cuaderno tiene muchas hojas");
        }
    }
};
console.log('Datos de mi cuaderno son:');
cuaderno.imprimirDatos();
console.log('Validando hojas:');
cuaderno.validar();
