//padre
class Vehiculo {
  protected llantas:number=0;
  protected peso:number=0;
  
  protected avanzar(){};
  protected retroceder(){};
  protected frenando(){};
}
//clase hija 1
class Auto extends Vehiculo{

  protected avanzar(){
    console.log("auto avanzando");
  }

  protected retroceder(){
    console.log("auto retrocediendo");
  }

  protected frenando(){
    console.log("auto frenando");
  }

  public ejecutar(){
    this.avanzar()
    this.retroceder()
    this.frenando
  }
}
//clase hija 2
class Moto extends Vehiculo{

  protected avanzar(){
    console.log("moto avanzando");
  }

  protected retroceder(){
    console.log("moto retrocediendo");
  }

  protected frenando(){
    console.log("moto frenando");
  }

  public ejecutar(){
    this.avanzar()
    this.retroceder()
    this.frenando
  }
}
//clase hija 3
class Camion extends Vehiculo{

  protected avanzar(){
    console.log("camion avanzando");
  }

  protected retroceder(){
    console.log("camion retrocediendo");
  }

  protected frenando(){
    console.log("camion frenando");
  }

  public ejecutar(){
    this.avanzar()
    this.retroceder()
    this.frenando
  }
}

let veh = new Vehiculo();
let aut = new Auto();
let mot = new Moto();
let cam = new Camion();

console.log('La clase padre es Vehiculo:');
console.log(veh);
console.log('');


console.log('La clase hija Auto:');
console.log(aut);
console.log('');


console.log('La clase hija Moto:');
console.log(mot);
console.log('');


console.log('La clase hija Camion:');
console.log(cam);