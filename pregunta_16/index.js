"use strict";
//padre
class Vehiculo {
    constructor() {
        this.llantas = 0;
        this.peso = 0;
    }
    avanzar() { }
    ;
    retroceder() { }
    ;
    frenando() { }
    ;
}
//clase hija 1
class Auto extends Vehiculo {
    avanzar() {
        console.log("auto avanzando");
    }
    retroceder() {
        console.log("auto retrocediendo");
    }
    frenando() {
        console.log("auto frenando");
    }
    ejecutar() {
        this.avanzar();
        this.retroceder();
        this.frenando;
    }
}
//clase hija 2
class Moto extends Vehiculo {
    avanzar() {
        console.log("moto avanzando");
    }
    retroceder() {
        console.log("moto retrocediendo");
    }
    frenando() {
        console.log("moto frenando");
    }
    ejecutar() {
        this.avanzar();
        this.retroceder();
        this.frenando;
    }
}
//clase hija 3
class Camion extends Vehiculo {
    avanzar() {
        console.log("camion avanzando");
    }
    retroceder() {
        console.log("camion retrocediendo");
    }
    frenando() {
        console.log("camion frenando");
    }
    ejecutar() {
        this.avanzar();
        this.retroceder();
        this.frenando;
    }
}
let veh = new Vehiculo();
let aut = new Auto();
let mot = new Moto();
let cam = new Camion();
console.log('La clase padre es Vehiculo:');
console.log(veh);
console.log('');
console.log('La clase hija Auto:');
console.log(aut);
console.log('');
console.log('La clase hija Moto:');
console.log(mot);
console.log('');
console.log('La clase hija Camion:');
console.log(cam);
