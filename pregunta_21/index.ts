function serie(n:number, x:number):void {

  let result: number[] = []
  let inicio: number = 1
  let final: number = n
  let iteracion : boolean = true

  while(n > result.length){
    if(iteracion){
      result.push(inicio++)
    }else {
      result.push(final--)
    }
    ((result.length % x) === 0)?iteracion = !iteracion : 0;
  }
  console.log(''+result);
}

console.log('test con 10, 4');
serie(10, 4)
console.log('');

console.log('test con 21, 5');
serie(21, 5)
console.log('');

console.log('test con 45, 8');
serie(45, 8)
console.log('');
