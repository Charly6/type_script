"use strict";
function serie(n, x) {
    let result = [];
    let inicio = 1;
    let final = n;
    let iteracion = true;
    while (n > result.length) {
        if (iteracion) {
            result.push(inicio++);
        }
        else {
            result.push(final--);
        }
        ((result.length % x) === 0) ? iteracion = !iteracion : 0;
    }
    console.log('' + result);
}
console.log('test con 10, 4');
serie(10, 4);
console.log('');
console.log('test con 21, 5');
serie(21, 5);
console.log('');
console.log('test con 45, 8');
serie(45, 8);
console.log('');
