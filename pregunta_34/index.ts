function volcarCadenas(cadena:string):void {
  let lista:string[] = cadena.split(' ')
  let result:string[] = []

  for (let index = 0; index < lista.length; index++) {
    result[index]=lista[lista.length-(index+1)]
  }
  console.log(result);
}

console.log('test con "hola mundo"');
volcarCadenas('hola mundo')
console.log('');

console.log('test con "hola amigo como estas este dia"');
volcarCadenas('hola amigo como estas este dia')
console.log('');

console.log('test con "me gusta mucho la musica variada"');
volcarCadenas('me gusta mucho la musica variada')
console.log('');
