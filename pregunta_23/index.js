"use strict";
function division(dividendo, divisor) {
    if ((divisor % 1 === 0) && (dividendo % 1 === 0)) {
        if (divisor != 0) {
            if (dividendo > divisor) {
                console.log(`El resultado es: ${dividendo / divisor}`);
            }
            else {
                console.log('El divisor no puede ser mayor que el dividendo');
            }
        }
        else {
            console.log('No se puede dividir entre 0');
        }
    }
    else {
        console.log('Solo se dividen numeros enteros');
    }
}
console.log('test con 12.23 / 2');
division(12.23, 2);
console.log('');
console.log('test con 12 / 0');
division(12, 0);
console.log('');
console.log('test con 8 / 26');
division(8, 26);
console.log('');
console.log('test con 24 / 12');
division(24, 12);
console.log('');
