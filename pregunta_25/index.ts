function numerosPrimos(n:number):void {
  let result: number[] = []
  let comprobando: number = 2;
  while(n > result.length){
    if(numerosPrimo(comprobando)){
      result.push(comprobando)
    }
    comprobando++
  }
  console.log(''+result);
}

function numerosPrimo(num:number):boolean {
  for (var i = 2; i < num; i++) {

    if (num % i === 0) {
      return false;
    }

  }
  return true;
}



console.log('test con 15');
numerosPrimos(15)
console.log('');

console.log('test con 4');
numerosPrimos(4)
console.log('');

console.log('test con 7');
numerosPrimos(7)
console.log('');
