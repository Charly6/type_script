"use strict";
//padre
class Figura {
    constructor() {
        this.area = 0;
        this.volumen = 0;
    }
}
//clase hija 1
class Cuadrado extends Figura {
    constructor() {
        super(...arguments);
        this.arista = 0;
    }
    calcularArea() {
        this.area = Math.pow(this.arista, 2);
        console.log(this.area);
    }
    calcularVolumen() {
        this.volumen = Math.pow(this.arista, 3);
        console.log(this.volumen);
    }
}
//clase hija 2
class Circulo extends Figura {
    constructor() {
        super(...arguments);
        this.radio = 0;
    }
    calcularArea() {
        this.area = (3.1416 * Math.pow(this.radio, 2));
        console.log(this.area);
    }
    calcularVolumen() {
        this.volumen = 3, 1416 * (4 / 3) * Math.pow(this.radio, 3);
        console.log(this.volumen);
    }
}
let fig = new Figura();
let cua = new Cuadrado();
let cir = new Circulo();
console.log('La clase padre es Figura:');
console.log(fig);
console.log('La clase hija Cuadrado:');
console.log(cua);
console.log('La clase hija Circulo:');
console.log(cir);
