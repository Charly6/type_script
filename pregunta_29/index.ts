function condatenacionCadenas(cadena1:string, cadena2:string):void {
  cadena1.length > cadena2.length? console.log(cadena1+cadena2):
  cadena2.length > cadena1.length? console.log(cadena2+cadena1):
  console.log('las cadenas no pueden tener el mismo tamaño');
}

console.log('test con "hola" - "mundo"');
condatenacionCadenas('hola', 'mundo')
console.log('');

console.log('test con "textoLargo" - "corto"');
condatenacionCadenas("textoLargo", "corto")
console.log('');

console.log('test con "igual" - "igual"');
condatenacionCadenas('igual', 'igual')
console.log('');