"use strict";
let persona = {
    nombre: "Charly",
    apellido: "meneces",
    imprimirDatos(opcional) {
        if (opcional === undefined) {
            console.log(`nombre: ${this.nombre}`);
            console.log(`apellido: ${this.apellido}`);
        }
    }
};
console.log("los datos de mi persona son:");
persona.imprimirDatos();
let barco = {
    nombre: "titanic",
    antiguedad: 50,
    imprimirDatos(opcional) {
        if (opcional === undefined) {
            console.log(`nombre: ${this.nombre}`);
            console.log(`antiguedad: ${this.antiguedad}`);
        }
    }
};
console.log("los datos de mi barco son:");
barco.imprimirDatos();
let aula = {
    estudiantes: 2,
    grado: 2,
    imprimirDatos(opcional) {
        if (opcional === undefined) {
            console.log(`estudiantes: ${this.estudiantes}`);
            console.log(`grado: ${this.grado}`);
        }
    }
};
console.log("los datos de mi aula son:");
aula.imprimirDatos();
let pizarra = {
    alto: 5,
    ancho: 5,
    imprimirDatos(opcional) {
        if (opcional === undefined) {
            console.log(`alto: ${this.alto}`);
            console.log(`ancho: ${this.ancho}`);
        }
    }
};
console.log("los datos de mi pizarra son:");
pizarra.imprimirDatos();
