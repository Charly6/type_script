//Persona
interface Persona {
  nombre:string;
  apellido:string;
  imprimirDatos(opcional? : string):void
}

let persona: Persona = {
  nombre: "Charly",
  apellido: "meneces",
  imprimirDatos(opcional?:string):void{
    if(opcional === undefined){
    console.log(`nombre: ${this.nombre}`)
    console.log(`apellido: ${this.apellido}`);
    }
  }
}
console.log("los datos de mi persona son:");
persona.imprimirDatos()

//Barco
interface Barco {
  nombre:string;
  antiguedad:number;
  imprimirDatos(opcional?: string):void
}

let barco : Barco = {
  nombre:"titanic",
  antiguedad:50,
  imprimirDatos(opcional?:string):void{
    if(opcional === undefined){
    console.log(`nombre: ${this.nombre}`)
    console.log(`antiguedad: ${this.antiguedad}`);
    }
  }
}
console.log("los datos de mi barco son:");
barco.imprimirDatos()
//Aula de clases
interface AulaDeClases {
  estudiantes:number;
  grado:number;
  imprimirDatos(opcional?: string):void
}

let aula : AulaDeClases = {
  estudiantes:2,
  grado:2,
  imprimirDatos(opcional?:string):void{
    if(opcional === undefined){
    console.log(`estudiantes: ${this.estudiantes}`)
    console.log(`grado: ${this.grado}`);
    }
  }
}
console.log("los datos de mi aula son:");
aula.imprimirDatos()
// Pizarra
interface Pizarra {
  alto:number;
  ancho:number;
  imprimirDatos(opcional?: string):void
}

let pizarra : Pizarra = {
  alto:5,
  ancho:5,
  imprimirDatos(opcional?:string):void{
    if(opcional === undefined){
    console.log(`alto: ${this.alto}`)
    console.log(`ancho: ${this.ancho}`);
    }
  }
}
console.log("los datos de mi pizarra son:");
pizarra.imprimirDatos()
