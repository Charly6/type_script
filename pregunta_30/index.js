function conteoVocales(cadena) {
    var result = {
        a: 0,
        e: 0,
        i: 0,
        o: 0,
        u: 0
    };
    for (var index = 0; index < cadena.length; index++) {
        cadena[index] === 'a' ? result.a++ :
            cadena[index] === 'e' ? result.e++ :
                cadena[index] === 'i' ? result.i++ :
                    cadena[index] === 'o' ? result.o++ :
                        cadena[index] === 'u' ? result.u++ :
                            '';
    }
    console.log(result);
}
console.log('test con "hola mundo"');
conteoVocales('hola mundo');
console.log('');
console.log('test con "doctor anulemelo"');
conteoVocales('doctor anulemelo');
console.log('');
console.log('test con "murcielago comiendo"');
conteoVocales('murcielago comiendo');
console.log('');
